﻿public class OpenStreetMapTagName
{
    // XML Tag Categories
    public const string emergencyTag = "emergency";
    public const string amenityTag = "amenity";

    public const string highwayTag = "highway";
    public const string trafficSignalsTag = "traffic_signals";
    public const string signalTag = "signal";
    public const string crossingTag = "crossing";

    // Amenity and Emergency item examples
    public const string postBoxItemName = "post_box";
    public const string fireHydrantItemName = "fire_hydrant";
}
